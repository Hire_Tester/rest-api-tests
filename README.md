# rest-api-tests
Automated testing of user manager REST API.
## Getting Started
The main purpose is creating automated tests for REST API.
## Setup
1. Install the latest version of [node.js](https://nodejs.org/). Once the install completes, test that node is installed by typing the following into the terminal `node -v && npm -v`. If you've got Node/npm already installed, you should update them to their latest versions. To update Node, the most reliable way is to download and install an updated installer package from their website (see link above). To update npm `npm install npm@latest -g`.
2. Clone this repository `git clone `.
3. Install dependencies `npm install`.
4. Run tests execute `npm test`.
5. Run `npm test -- ibis.spec.js` to run a specific test suite.

## Project structure
* **__tests__/** - files with tests. Our setup uses [Jest].
* **validation/** - files with schemas for validation.
* **config/** - a directory for storing routes, configs and so on.

## Built With
- [Supertest](https://github.com/visionmedia/supertest) - Super-agent driven library for testing node.js HTTP servers using a fluent API.
- [Jest](https://jestjs.io/en/) - Jest is a delightful JavaScript Testing Framework with a focus on simplicity.