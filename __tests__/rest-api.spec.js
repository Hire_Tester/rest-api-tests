const supertest = require('supertest');
const { newUserSchema } = require('../validation/newUserSchema');
const { updateUserSchema } = require('../validation/updateUserSchema');
const { authUserSchema } = require('../validation/authUserSchema');
const { newAlarmGroupSchema } = require('../validation/newAlarmGroup');

const { constants } = require('../config/constants');
const {
    login,
    newUser,
    existingUser,
    updatedUser,
    defaultsProps,
    group,
    alarmGroup,
    newAlarmGroup,
    moveUsers,
} = constants;
const { routes } = require('../config/routes');
const {
    base,
    public: { authentication },
    private: { users, defaults, groups, alarmGroups },
} = routes;

let baseURL = supertest(base);
const existingUserID = existingUser.id;
const updatedUserID = updatedUser.id;
const groupID = group.id;
const alarmGroupID = alarmGroup.id;

describe('REST API', () => {
    let session = null;
    let usersList = [];
    let userID = null;

    test('POST /authentication', async () => {
        const { error } = authUserSchema.validate(login);
        await expect(error).toBeUndefined();

        const { text, status } = await baseURL
            .post(authentication)
            .send(login)
            .set('Content-Type', 'application/json')
            .set('Accept', '*/*');
        session = text;
        await expect(status).toBe(200);
        await expect(session).toContain('Basic');
    });

    test('GET /users', async () => {
        const { status, type, body } = await baseURL.get(users).set('Authorization', session);
        await expect(status).toBe(200);
        await expect(type).toBe('application/json');
        await expect(body).toBeArray();
    });

    test('POST /users', async () => {
        const { body: id, status } = await baseURL
            .post(users)
            .set('Authorization', session)
            .set('Accept', '*/*')
            .set('Content-Type', 'application/json')
            .send(newUser);
        await expect(status).toBe(200);

        const { error } = newUserSchema.validate(newUser);
        await expect(error).toBeUndefined();

        userID = { id: id };
        const { body: usersList } = await baseURL.get(users).set('Authorization', session);
        await expect(usersList).toEqual(await expect.arrayContaining([expect.objectContaining(userID)]));
    });

    test('GET /users/{id}', async () => {
        const { status, type, body } = await baseURL.get(`${users}/${existingUserID}`).set('Authorization', session);
        await expect(status).toBe(200);
        await expect(type).toBe('application/json');
        await expect(body).toBeObject();
        await expect(body).toContainValue(existingUserID);
    });

    test('PUT /users', async () => {
        const { error } = updateUserSchema.validate(updatedUser);
        await expect(error).toBeUndefined();

        // get 'firstName' property's value before resource updating
        const { body: bodyFromGetReq } = await baseURL.get(`${users}/${updatedUserID}`).set('Authorization', session);
        const firstNameBeforeUpdate = bodyFromGetReq.firstName;
        // update resourse with a new data
        const { status } = await baseURL
            .put(users)
            .set('Authorization', session)
            .set('Accept', '*/*')
            .set('Content-Type', 'application/json')
            .send(updatedUser);
        await expect(status).toBe(200);
        // get 'firstName' property's value after resource updating
        const { body: bodyFromPostReq } = await baseURL.get(`${users}/${updatedUserID}`).set('Authorization', session);
        const firstNameAfterUpdate = bodyFromPostReq.firstName;
        await expect(bodyFromPostReq).toBeObject();
        await expect(bodyFromPostReq).not.toContainValue(firstNameBeforeUpdate);
        await expect(bodyFromPostReq).toContainValue(firstNameAfterUpdate);
    });

    test('DELETE /users', async () => {
        const url = `${users}?userId=${userID.id}`;
        const { status } = await baseURL
            .delete(url)
            .set('Authorization', session)
            .set('Accept', '*/*');
        await expect(status).toBe(200);
        // verify that a resource doesn't exist anymore
        const { body } = await baseURL.get(users).set('Authorization', session);
        usersList = body;
        await expect(usersList).toEqual(await expect.arrayContaining([expect.not.objectContaining(userID)]));
    });

    test('GET /defaults', async () => {
        const { status, type, body } = await baseURL.get(defaults).set('Authorization', session);
        await expect(status).toBe(200);
        await expect(type).toBe('application/json');
        await expect(body).toBeObject();
        await expect(body).toContainAllKeys(defaultsProps);
    });

    test('GET /groups/{groupId}/users', async () => {
        const { status, type, body } = await baseURL.get(`${groups}/${groupID}/users`).set('Authorization', session);
        await expect(status).toBe(200);
        await expect(type).toBe('application/json');
        await expect(body).toBeArray();
    });

    test('GET /alarmGroups/{alarmGroupId}/users', async () => {
        const { status, type, body } = await baseURL
            .get(`${alarmGroups}/${alarmGroupID}/users`)
            .set('Authorization', session);
        await expect(status).toBe(200);
        await expect(type).toBe('application/json');
        await expect(body).toBeArray();
    });

    test('POST /alarmGroups', async () => {
        const { error } = newAlarmGroupSchema.validate(newAlarmGroup);
        await expect(error).toBeUndefined();

        const { status } = await baseURL
            .post(alarmGroups)
            .set('Authorization', session)
            .set('Accept', '*/*')
            .set('Content-Type', 'application/json')
            .send(newAlarmGroup);
        await expect(status).toBe(200);
    });

    test('POST /alarmGroups/move-users', async () => {
        // get first user's ID from alarmGroup with ID=1
        const { body } = await baseURL.get(`${alarmGroups}/${alarmGroupID}/users`).set('Authorization', session);
        // push this ID to the users array
        moveUsers.users.push(body[0].id);
        // move this user to alarmGroup with ID=2
        const { status } = await baseURL
            .post(`${alarmGroups}/move-users`)
            .set('Authorization', session)
            .set('Accept', '*/*')
            .set('Content-Type', 'application/json')
            .send(moveUsers);
        await expect(status).toBe(200);
        // verify that user is moved
        const { body: result } = await baseURL
            .get(`${alarmGroups}/${moveUsers.groupId}/users`)
            .set('Authorization', session);
        await expect(result).toEqual(
            await expect.arrayContaining([expect.objectContaining({ id: moveUsers.users[0] })]),
        );
    });
});
