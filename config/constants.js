const faker = require('faker');

module.exports.constants = {
    login: {
        userName: '',
        password: '',
    },
    newUser: {
        resetPassword: true,
        active: true,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        userName: faker.internet.userName(),
        password: faker.internet.password(),
        startTime: 0,
        endTime: 1440,
        defaultGroup: { id: 1, name: 'root' },
        groups: [],
        alarmGroups: [],
        primaryEmail: '',
        emails: [],
        permissions: [],
    },
    existingUser: {
        id: 299,
    },
    updatedUser: {
        id: 299,
        resetPassword: true,
        active: true,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        userName: faker.internet.userName(),
        password: faker.internet.password(),
        startTime: 0,
        endTime: 1440,
        defaultGroup: { id: 1, name: 'root' },
        groups: [],
        alarmGroups: [],
        primaryEmail: 'mprado@ibs-cal.com',
        emails: [],
        permissions: [],
    },
    defaultsProps: ['mailServer', 'permissions', 'groups', 'alarmGroups'],
    group: {
        id: 1,
    },
    alarmGroup: {
        id: 1,
    },
    newAlarmGroup: {
        name: faker.random.word(),
    },
    moveUsers: {
        groupId: 2,
        groupName: 'New Alarm Group',
        users: [],
    },
};
