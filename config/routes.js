module.exports.routes = {
    base: '',
    public: {
        authentication: '/api/Authentication',
    },
    private: {
        users: '/api/Users',
        defaults: '/api/Defaults',
        groups: '/api/Groups',
        alarmGroups: '/api/alarmGroups',
    },
};
