const fs = require('fs');

const dir = './TestResults';

try {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        fs.rename('./junit.xml', `${dir}/junit.xml`, e => {
            if (e) console.error(e);
        });
    }
} catch (err) {
    console.error(err);
}
