const Joi = require('@hapi/joi');

module.exports = {
    newUserSchema: Joi.object().keys({
        resetPassword: Joi.bool(),
        active: Joi.bool(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        userName: Joi.string().required(),
        password: Joi.string().required(),
        startTime: Joi.number().integer(),
        endTime: Joi.number().integer(),
        defaultGroup: Joi.object({
            id: Joi.number().required(),
            name: Joi.string().required(),
        }).required(),
        groups: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                name: Joi.string(),
            }),
        ),
        alarmGroups: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                name: Joi.string(),
            }),
        ),
        primaryEmail: Joi.string()
            .email()
            .required(),
        emails: Joi.array().items(Joi.string()),
        permissions: Joi.array().items(
            Joi.object({
                value: Joi.string(),
                access: Joi.string(),
            }),
        ),
    }),
};
