const Joi = require('@hapi/joi');

module.exports = {
    newAlarmGroupSchema: Joi.object().keys({
        name: Joi.string().required(),
    }),
};
