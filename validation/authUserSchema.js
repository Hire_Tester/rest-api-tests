const Joi = require('@hapi/joi');

module.exports = {
    authUserSchema: Joi.object().keys({
        userName: Joi.string().required(),
        password: Joi.string().required(),
    }),
};
